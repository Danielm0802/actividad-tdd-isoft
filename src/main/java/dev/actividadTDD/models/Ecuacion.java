package dev.actividadTDD.models;

import lombok.Data;
import org.apache.commons.math3.stat.regression.SimpleRegression;

import java.text.DecimalFormat;
import java.util.List;

@Data
public class Ecuacion {
    private String cadena;
    private SimpleRegression regression;
    private String stringEcuacion;

    public Ecuacion(String cadena) {
        this.cadena = cadena;
        regression = new SimpleRegression();
    }

    public void generateRegression(){

        List<String> splitted = List.of(this.cadena.trim().split("/"));

        for (String cadena:
                splitted) {
            int index1 = cadena.indexOf('(')+1;
            int index2 = cadena.indexOf(')');
            String[] par = cadena.substring(index1,index2).split(",");
            this.regression.addData( Double.parseDouble(par[0]),Double.parseDouble(par[1]) );
        }

    }

    public void parseEcuacion(){
        double beta0 = this.regression.getIntercept();
        double beta1 = this.regression.getSlope();
        DecimalFormat decimalFormat =  new DecimalFormat("#.####");
        this.stringEcuacion = "y = "+decimalFormat.format(beta0) + " + " + decimalFormat.format(beta1) + "x";
    }

}
