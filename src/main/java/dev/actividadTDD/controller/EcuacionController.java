package dev.actividadTDD.controller;

import dev.actividadTDD.models.Ecuacion;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;


@Controller
@RequestMapping()
public class EcuacionController {

    private static Ecuacion ecuacion;

    @GetMapping("")
    public String entradaDatos(Model model){
        return "entrada_datos";
    }

    public boolean validarEntrada(String entrada){
        return !entrada.trim().equals("");
    }

    public boolean validarEstructuraCadena(String cadena){
        String cadenaTrimed = cadena.trim();
        int indexLastChar = cadenaTrimed.length() - 1;

        if(cadenaTrimed.indexOf(',') == -1){
            return false;
        }

        return cadenaTrimed.charAt(0) == '(' && cadenaTrimed.charAt(indexLastChar) == ')';
    }

    public int cantidadCaracter(char caracter, String cadena){
        String cadenaTrimed = cadena.trim();
        int cantidad = 0;
        for (int i = 0; i < cadenaTrimed.length(); i++) {
            if (cadenaTrimed.charAt(i) == caracter){
                cantidad++;
            }
        }
        return cantidad;
    }

    public boolean canSplitCadena(String cadena) {
        String cadenaTrimed = cadena.trim();
        if (cadenaTrimed.indexOf('/') == -1) {
            return false;
        }

        ArrayList<Integer> slashIndices = new ArrayList<>();
        for (int i = 0; i < cadenaTrimed.length(); i++) {
            if (cadena.charAt(i) == '/') {
                slashIndices.add(i);
            }
        }

        //si tenemos 2 slash debemos tener 3 ( y 3 )

        if ( cantidadCaracter('(', cadenaTrimed) != slashIndices.size() + 1 || cantidadCaracter(')', cadenaTrimed) != slashIndices.size() + 1 ){
            return false;
        }
        return true;
    }


    @PostMapping("calcular_regresion")
    public String calcular_regresion(@RequestParam(required = true) String entrada){

        if ( !validarEntrada(entrada) || !validarEstructuraCadena(entrada) || !canSplitCadena(entrada) ){
            return "redirect:/error";
        }
        ecuacion = new Ecuacion(entrada);
        ecuacion.generateRegression();
        ecuacion.parseEcuacion();
        return "redirect:/resultado";
    }

    @GetMapping("resultado")
    public String resultado(Model model){
        model.addAttribute("ecuacion", ecuacion.getStringEcuacion() );
        return "resultado";
    }


}
