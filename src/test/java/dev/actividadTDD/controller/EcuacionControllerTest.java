package dev.actividadTDD.controller;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName("Test de validacion de entrada de datos")
public class EcuacionControllerTest {


        EcuacionController ecuacionController;

        @BeforeEach
        void setUp() {
            ecuacionController = new EcuacionController();
        }

        @Test
        @DisplayName("Validar cadena vacia")
        public void testCadenaNoVacia(){
            assertTrue(ecuacionController.validarEntrada("(2, 0.51824)"));
        }

        @Test
        @DisplayName("Verificar estructura cadena")
        public void testEstructuraCadena(){
            assertTrue(ecuacionController.validarEstructuraCadena(" (1, 0.53228)  "));
            assertFalse(ecuacionController.validarEstructuraCadena(" (1   0.53228)  "));
            assertFalse(ecuacionController.validarEstructuraCadena(" 1 ,  0.53228  "));
        }

        @Test
        @DisplayName("Validar cadena es divisible")
        public void testCadenaDivisible(){
            String divisible = "(2, 0.51824) / (3,0.53829)";
            assertTrue(ecuacionController.canSplitCadena(divisible));
        }

        @Test
        @DisplayName("Validar cadena no divisible")
        public void testCadenaNoDivisible(){
            String noDivisible = "(2, 0.51824)";
            assertFalse(ecuacionController.canSplitCadena(noDivisible));
        }

        @Test
        @DisplayName("Validar formato de cadena incorrecto")
        public void testCadenaDivisibleFormatoIncorrecto(){
            String formatoIncorrecto = "(2, 0.51824) / (3,0.53829) /";
            assertFalse(ecuacionController.canSplitCadena(formatoIncorrecto));
        }


        @Test
        @DisplayName("Validar cantidad caracteres")
        public void testCantidadCaracteres(){
            String cadena = "(2, 0.51824) / (3, 0.215125) / (4, 0.235231)";
            assertEquals(3, ecuacionController.cantidadCaracter( '(' , cadena));
            assertEquals(3, ecuacionController.cantidadCaracter( ')' , cadena));
        }


        @AfterEach
        void tearDown() {
        }

}
